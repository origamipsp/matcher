const { parentPort } = require('worker_threads')
const axios = require('axios')
const cheerio = require('cheerio')
const { distance } = require('fastest-levenshtein')
const db = require('../models/index.js')
const { Op } = require("sequelize");
const c = require('ansi-colors');

const Scammer = db.scammer
const min = 100
const max = 195

const delay = ms => new Promise(resolve => setTimeout(resolve, ms))
function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}




async function getPagesCount(agency) {
  try {
    const html = await axios.get(`https://www.indeed.com/jobs?q=${agency}&filter=0&start=0`)
    const $ = cheerio.load(html.data)
    let jobsCount = ""
    $('#searchCountPages').each((i, elem) => {
      jobsCount += `${$(elem).text()}`
    })
    jobsCount = jobsCount.replace(/\s/g, '');
    let start = jobsCount.indexOf("f")
    let end = jobsCount.indexOf("j")
    jobsCount = jobsCount.slice(start + 1, end)
    jobsCount = parseInt(jobsCount)
    let pagesCount = Math.floor(jobsCount / 15)

    return pagesCount
    //.log("------", jobsCount)
    //console.log("------", pagesCount)
  } catch (error) {
    console.log(error)
  }
}









//парсим url всех потенциальных скамеров, закидываем их в массив  obj = 1 вакансия
async function parseJobsUrl(obj) {

  //progress.setHeader('Working ...').start();
  console.log("Worker is working...")
  console.log("obj---", obj)

  let potentialScammerList = []
  
  try {
   let pageCount = await getPagesCount(obj.title)
    for (let j = 0; j <= pageCount; j++) {
      try {
       // await delay(getRandomInt(min, max))
        const html = await axios.get(`https://www.indeed.com/m/jobs?q=${obj.title}&filter=0&start=${[j]*2}0`)
        const $ = cheerio.load(html.data)
        $('div.heading4.color-text-primary.singleLineTitle.tapItem-gutter > h2 > a').each((i, elem) => {

          let vacancyId = `${$(elem).attr('href')}`
          let cutId = vacancyId.substring(vacancyId.indexOf('=') + 1, vacancyId.indexOf('&'));

          if(cutId.length > 16){
            cutId = cutId.substring(cutId.lastIndexOf('-')+1, cutId.lastIndexOf('?'))
          }
          
          
          if (obj.url !== cutId) {

            potentialScammerList.push(cutId)
            console.log("add to array----", cutId )
          }
        })
        


      } catch (error) {
        console.log(`ParseJobsUrl error: ${error.message}`)
      }
    }

  
  } catch (e) {
    console.log(`Out of page range error: ${e.message}`)
  }
  await parseAllFrompotentialScammer([...new Set(potentialScammerList)], obj)

}


//парсим всё нужное нам содержимое  из каждой url массива потенциальных скамеров
async function parseAllFrompotentialScammer(urls, obj) {

  for (let i = 0; i < urls.length; i++) {
     console.log("URL START_______:", urls[i])
    try {
      //await delay(getRandomInt(min, max + 350))
      const html = await axios.get(`https://www.indeed.com/viewjob?jk=${urls[i]}`)
      const $ = cheerio.load(html.data)

      let title = ""
      $('.jobsearch-JobInfoHeader-title').each((i, elem) => {
        title += `${$(elem).text()}`
      })
      //Удаляем мусор из тайтла и приводим его к необходимому виду
      // пример "Delivery Manager/ Lead", в запросе это должно выглядеть так "Delivery-Manager-Lead"
      title = title.replace(/[^a-zа-яё0-9\s]/gi, '').split(' ').filter(Boolean).join('-')

      let description = ""
      $('#jobDescriptionText').each((i, elem) => {
        description += `${$(elem).text()}`
      })

      description = description.split(' ').join('')
      description = description.replace(/\r?\n/g, "")
      if (description.length >= 499) {
        description = description.substring(0, 499)
      }

      let companyName = ""
      $('.jobsearch-CompanyReview--heading').each((i, elem) => {
        companyName += `${$(elem).text()}`
      })

      let companyUrl = ""
      $('.jobsearch-DesktopStickyContainer-companyrating > div:nth-child(2) > div > a').each((i, elem) => {
        companyUrl += `${$(elem).attr('href')}`
      })



      console.log("!!!!!title - ",  title)
      console.log("!!!!!description - ",  description)
      console.log("!!!!!companyName - ",  companyName)
      console.log("!!!!!url - ",  companyUrl)

      // проверяем расстояние левинштайна между описанием вакансси заказчика и описанием каждого потенциального заказчика
      // если удовлетворяет условию - записываем в бд найденного скамера
      let diff = distance(obj.description, description) / obj.description.length
      // console.log("includes___________: ", obj.companies.includes(companyName))
      // console.log("scammerName________: ", companyName)
      // console.log("companyName________: ", obj.companies)
      if (diff < 0.3 && !obj.companies.includes(companyName)) {
        console.log(c.red('SCAMMER!!!!!!!!!!!!!!!!!!!!!!!!!!'));
        const [scammer, created] = await Scammer.findOrCreate({
          where: {
            //url: urls[i]
            [Op.or]: [
              { url: urls[i] },
              { companyName: companyName }
            ]
          },
          defaults: { vacancyId: obj.id, url: urls[i], companyName }
        });

      }
    }
    catch (e) {
      console.log(`parseAllFrompotentialScammer error: ${e.message} on url ___:${urls[i]}`)
    }
  }
}

// Основной поток передаст нужные вам данные
// через этот прослушиватель событий.
parentPort.on('message', async (param) => {
  const result = await parseJobsUrl(param);
  // Access the workerData.
  // return the result to main thread.
  parentPort.postMessage(`feels good`);
});
