const config = require('config')
const Sequelize = require('sequelize')
const dbConfig = config.get('DBConn')

//console.log("_______________________", dbConfig)

const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD,  {
    host: dbConfig.HOST,
    dialect: dbConfig.dialect,
    // operatorsAliases: false,
    logging: dbConfig.logging,

    pool: {
        max: dbConfig.pool.max,
        min: dbConfig.pool.min,
        acquire: dbConfig.pool.acquire,
        idle: dbConfig.pool.idle
    },
})

const db = {}

db.Sequelize = Sequelize
db.sequelize = sequelize

// COMPANY
db.company = require('./company.model')(sequelize, Sequelize)
db.vacancy = require('./vacancy.model')(sequelize, Sequelize)
db.scammer = require('./scammer.model')(sequelize, Sequelize)

db.company.hasMany(db.vacancy, {foreignKey: 'companyId' })
db.vacancy.belongsTo(db.company, { foreignKey: 'companyId', onDelete: 'CASCADE' })

db.vacancy.hasMany(db.scammer, {foreignKey: 'vacancyId' })
db.scammer.belongsTo(db.vacancy, { foreignKey: 'vacancyId', onDelete: 'CASCADE' })


module.exports = db