const { StaticPool } = require('node-worker-threads-pool')
const os = require('os')
const db = require('../models/index.js')
//const fs = require('fs')
const path = require('path')
const axios = require('axios')
const cheerio = require('cheerio')
const { Op } = require("sequelize");
//const { table } = require('console')


const Vacancy = db.vacancy
const Company = db.company
const Scammer = db.scammer



//путь к воркеру
const filePath = path.join(path.dirname(require.main.filename), 'workers', 'worker_indeed.js');
const numCPUs = os.cpus().length

// //создаем пулл воркеров
// const pool = new StaticPool({
//   size: 6,
//   task: filePath,
//   workerData: 'workerData!'
// });






// async function getPagesCount(agency) {
//   try {
//     const html = await axios.get(`https://www.indeed.com/jobs?q=${agency}&filter=0&start=0`)
//     const $ = cheerio.load(html.data)
//     let jobsCount = ""
//     $('#searchCountPages').each((i, elem) => {
//       jobsCount += `${$(elem).text()}`
//     })

//     jobsCount = jobsCount.replace(/\s/g, '');
//     let start = jobsCount.indexOf("f")
//     let end = jobsCount.indexOf("j")
//     jobsCount = jobsCount.slice(start + 1, end)
//     jobsCount = parseInt(jobsCount)
//     let pagesCount = Math.floor(jobsCount / 15)

//     return pagesCount
//     //.log("------", jobsCount)
//     //console.log("------", pagesCount)
//   } catch (error) {`
//     console.log(error)
//   }
// }




async function getPagesCount(agency) {
  let pagesCount
  do {

    try {
      const html = await axios.get(`https://www.indeed.com/cmp/${agency}/jobs?start=150`)
      const $ = cheerio.load(html.data)
      let jobsCount = ""
      $('div > div.css-18fh2jl.eu4oa1w0 > h2 > span').each((i, elem) => {
        jobsCount += `${$(elem).text()}`
      })
      jobsCount = jobsCount.replace(/\s/g, '');
      jobsCount = jobsCount.substring(0, jobsCount.indexOf('j'))
      jobsCount = jobsCount.replace(",", '');
      jobsCount = parseInt(jobsCount)
      pagesCount = Math.ceil(jobsCount / 150)

      console.log("------", jobsCount)
      console.log("------", pagesCount)



    } catch (error) {
      console.log(error)
    }

  } while (!pagesCount || isNaN(pagesCount));
  return pagesCount

}


async function parsePage(data) {
  let list = []
  do {
    console.log("cycle started")
    const $ = cheerio.load(data)
    try {
      $('div.css-150v3kv.eu4oa1w0 > ul > li').each((i, elem) => {
        console.log(')))))(((((')
        let vacancyId = `${$(elem).attr('data-tn-entityid')}`
        //var cutId = vacancyId.substring(vacancyId.indexOf('=') + 1, vacancyId.indexOf('&'));
        var cutId = vacancyId.substring(vacancyId.indexOf(',') + 1, vacancyId.lastIndexOf(','));
        console.log("_________!!!!!", cutId)
        list.push(cutId)
        console.log("cycle stoped")
      })
    } catch (error) {
      console.log(error)
    }
    

  } while (list.length === 0)

  return list

}


//парсим url всех вакансий нужной компании, закидываем их в массив
async function addCompanies(req, res) {
  const idList = []
  try {

    console.log(req.body.companies)
    
    let agencyArr = req.body.companies
    for (let j = 0; j < agencyArr.length; j++) {
      let pageCount = await getPagesCount(agencyArr[j])
      console.log("///////", pageCount)
      for (let i = 0; i < pageCount; i++) {

        try {
          const html = await axios.get(`https://www.indeed.com/cmp/${agencyArr[j]}/jobs?start=${[i] * 150}`)
          console.log(`add - ${[i] * 150} `, agencyArr[j])
          
          const $ = cheerio.load(html.data)
          $('div.css-150v3kv.eu4oa1w0 > ul > li').each((i, elem) => {
            let vacancyId = `${$(elem).attr('data-tn-entityid')}`
            //var cutId = vacancyId.substring(vacancyId.indexOf('=') + 1, vacancyId.indexOf('&'));
            var cutId = vacancyId.substring(vacancyId.indexOf(',') + 1, vacancyId.lastIndexOf(','));
            idList.push(cutId)
          })
          //idList = idList.concat(await parsePage(html.data))
          console.log(i)
          console.log(idList)
        }
        catch (e) {
          console.log(e)
        }
      }
    }
    // console.log("PARSING ALL")
  
    await parseAll([...new Set(idList)]) // сдесь передаем все url вакансий, всех компаний переданных в post запросе
    res.status(200).json({ message: 'ok' })
  } catch (error) {
    res.status(500).json({ message: error.message })
  }
}

//парсим всё нужное нам содержимое из каждой url массива
async function parseAll(idList) {
  // console.log("PARSING ALL")
  // console.log(idList)
  
  // console.log(idList.length)
  for (let i = 0; i < idList.length; i++) {

    try {
      const html = await axios.get(`https://www.indeed.com/viewjob?jk=${idList[i]}`)
      const $ = cheerio.load(html.data)

      let title = ""
      $('.jobsearch-JobInfoHeader-title').each((i, elem) => {
        title += `${$(elem).text()}`
      })

      //Удаляем мусор из тайтла и приводим его к необходимому виду
      // пример "Delivery Manager/ Lead", в запросе это должно выглядеть так "Delivery-Manager-Lead"
      title = title.replace(/[^a-zа-яё0-9\s]/gi, '').split(' ').filter(Boolean).join('-')

      let description = ""
      $('#jobDescriptionText').each((i, elem) => {
        description += `${$(elem).text()}`
      })

      description = description.split(' ').join('')
      description = description.replace(/\r?\n/g, "")
      if (description.length >= 499) {
        description = description.substring(0, 499)
      }

      let companyName = ""
      $('.jobsearch-DesktopStickyContainer-companyrating > div:nth-child(2) > div > a').each((i, elem) => {
        companyName += `${$(elem).text()}`
      })

      let companyUrl = ""
      $('.jobsearch-DesktopStickyContainer-companyrating > div:nth-child(2) > div > a').each((i, elem) => {
        companyUrl += `${$(elem).attr('href')}`
        companyUrl = companyUrl.substring(0, companyUrl.indexOf("?"))
      })

      const [company, created] = await Company.findOrCreate({
        where: {
          url: companyUrl
        },
        defaults: { companyName, url: companyUrl }
      });

      const [vacancy, created2] = await Vacancy.findOrCreate({
        where: {
          url: idList[i]
        },
        defaults:  { companyId: company.id, title, url: idList[i], description } 
      });

      //const vacancy = await Vacancy.create({ companyId: company.id, title, url: idList[i], description })

    } catch (e) {
      console.log(e)
    }
  }
}





// считываем все с бд и передаем параметры в воркеры
async function findScammers(req, res) {

  //создаем пулл воркеров
  const pool = new StaticPool({
    size: 6,
    task: filePath,
    workerData: 'workerData!'
  });


  try {
    if (!+process.env.WORKING) {
      process.env.WORKING = 1
      const start = new Date().getTime();
      const vacancies = await Vacancy.findAll()
      const companies = await Company.findAll()
      let companiesNames = []
      let counter = 0;
      for (let j = 0; j < companies.length; j++) {
        companiesNames.push(companies[j].dataValues.companyName)
      }

      for (let i = 0; i < vacancies.length; i++) {

        (async () => {

          const result = await pool.exec({ id: vacancies[i].id, title: vacancies[i].title, url: vacancies[i].url, description: vacancies[i].description, companies: companiesNames });

          console.log("___________________:", result)
          counter++;
          if (counter >= vacancies.length - 1) {
            await pool.destroy()
            process.env.WORKING = 0
            const end = new Date().getTime();
            console.log(`SecondWay: ${end - start}ms`);
          }
        })();
      }

      res.status(200).json({ message: 'Started' })
    } else if (process.env.WORKING) {
      res.status(200).json({ message: "Working..." })
    }
  } catch (error) {
    res.status(500).json({ message: error.message })
  }
}

//получаем результаты по post запросу в котором передаем url компании и возвращаем массив обьектов
async function getResults(req, res) {
 
  try {
    const result = await Company.findAll({
      where: { url: req.body.url },
      include: [{
        model: Vacancy,
        include: [{
          model: Scammer,
        }]
      }]
    })
    // result[0].dataValues.companyName - название компании заказчика
    // result[0].dataValues.url - юрл компании заказчика
    // result[0].dataValues.table_vacancies[0].dataValues.url - юрл родительской вакансии заказчика
    // result[0].dataValues.table_vacancies[0].dataValues.table_scammers[0].dataValues.url - url вакансии скамера
    // result[0].dataValues.table_vacancies[0].dataValues.table_scammers[0].dataValues.companyName - название компании скамера
    let results = []
    for (let i = 0; i < result.length; i++) {
      for (let j = 0; j < result[i].dataValues.table_vacancies.length; j++) {
        for (let k = 0; k < result[i].dataValues.table_vacancies[j].dataValues.table_scammers.length; k++) {

          if (typeof result[i].dataValues.table_vacancies[j].dataValues.table_scammers[k] !== 'undefined') {
            results.push(
              {
                "clientName": result[i].dataValues.companyName,
                "clientUrl": result[i].dataValues.url,
                "parentVacancyUrl": result[i].dataValues.table_vacancies[j].dataValues.url,
                "scammerVacancyUrl": result[i].dataValues.table_vacancies[j].dataValues.table_scammers[k].dataValues.url,
                "scammerCompanyName": result[i].dataValues.table_vacancies[j].dataValues.table_scammers[k].dataValues.companyName,
              }
            )
          } else {
            results.push(
              {
                "clientName": result[i].dataValues.companyName,
                "clientUrl": result[i].dataValues.url,
                "parentVacancyUrl": result[i].dataValues.table_vacancies[j].dataValues.url,
                "scammerVacancyUrl": "",
                "scammerCompanyName": "",
              }
            )
          }
        }
      }
    }


    res.status(200).json({ results })
  } catch (error) {
    res.status(500).json({ message: error.message })
  }
}

module.exports = {
  addCompanies, findScammers, getResults
}