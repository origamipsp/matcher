const { StaticPool } = require('node-worker-threads-pool')
const os = require('os')
const db = require('../models/index.js')
const fs = require('fs')
const path = require('path')
const axios = require('axios')
const cheerio = require('cheerio')
const { Op } = require("sequelize");
const { table } = require('console')


const Vacancy = db.vacancy
const Company = db.company
const Scammer = db.scammer
let isWorking = false

//путь к воркеру
const filePath = path.join(path.dirname(require.main.filename), 'workers', 'worker_monster.js');
const numCPUs = os.cpus().length

//создаем пулл воркеров
const pool = new StaticPool({
  size: 2,
  task: filePath,
  workerData: 'workerData!'
});

//получаем количество страниц для парсинга
async function getPagesCount(agency) {
  try {
    const html = await axios.get(`https://www.reed.co.uk/jobs/${agency}?pageno=1`)
    const $ = cheerio.load(html.data)
    let jobsCount = ""
    $('div.row.search-results > div.col-sm-8.col-md-9.results-container > div.search-results-footer > div:nth-child(1) > div.row-top.hidden-xs > div > div > div').each((i, elem) => {
      jobsCount += `${$(elem).text()}`
    })
    jobsCount = jobsCount.replace(/\s/g, '');
    let start = jobsCount.indexOf("f")
    let end = jobsCount.indexOf("j")
    jobsCount = jobsCount.slice(start + 1, end)
    jobsCount = parseInt(jobsCount)
    let pagesCount = Math.ceil(jobsCount / 25)
    return pagesCount

  } catch (error) {
    console.log(error)
  }
}

//getPagesCount("experis-ltd/p24927")


// парсим название компании и записываем в бд
async function parseCompanyName() {
  try {
    const html = await axios.get(`https://www.reed.co.uk/company-profile/${agencyArr[j]}`)
    const $ = cheerio.load(html.data)
    let companyName = ""
    $('div:nth-child(2) > div > section.profile__information > h1 > span').each((i, elem) => {
      companyName += `${$(elem).text()}`
    })
    companyName = companyName.replace("Working at ", "")
    const [company, created] = await Company.findOrCreate({
      where: { url: agencyArr[j] },
      defaults: { companyName, url: agencyArr[j] }
    });
  } catch (error) {
    console.log(error)
  }
}

//парсим url всех вакансий нужной компании, закидываем их в массив
async function addCompanies(req, res) {
  let urlList = []
  let agencyArr = req.body.companies
  for (let j = 0; j < agencyArr; j++) {

    await parseCompanyName(agencyArr[j]) // вынесен парсинг названия компании и запись в бд

    const pageCount = await getPagesCount(agencyArr[j])

    for (let i = 1; i <= pageCount; i++) {

      try {
        const html = await axios.get(`https://www.reed.co.uk/jobs/${agencyArr[j]}?pageno=${i}`)
        const $ = cheerio.load(html.data)
        $('div > div.col-sm-12.col-md-9.details > header > h3 > a').each((i, elem) => {

          let cutUrl = `https://www.reed.co.uk${$(elem).attr('href')}`
          let symbol = cutUrl.indexOf("?")
          cutUrl = cutUrl.substring(0, symbol)
          urlList.push(cutUrl)

        })
      } catch (error) {
        console.log(error)
      }
    }

  }

  await parseAll(urlList) // сдесь передаем все url вакансий, всех компаний переданных в post запросе
}

//парсим всё нужное нам содержимое из каждой url массива
async function parseAll(urls) {

  for (let i = 0; i < urls.length; i++) {
    try {
      const html = await axios.get(urls[i])
      const $ = cheerio.load(html.data)

      let title = ""
      $('div.job-details.row > div.col-xs-12.col-sm-12.col-md-9 > article > div > header > div.col-xs-12 > h1').each((i, elem) => {
        title += `${$(elem).text()}`
      })
      //Удаляем мусор из тайтла и приводим его к необходимому виду
      // пример "Delivery Manager/ Lead", в запросе это должно выглядеть так "Delivery-Manager-Lead"
      title = title.replace(/[^a-zа-яё0-9\s]/gi, '').split(' ').filter(Boolean).join('-')

      let description = ""
      $('div.job-details.row > div.col-xs-12.col-sm-12.col-md-9 > article > div > div.description-container.row > div > div.description > span').each((i, elem) => {
        description += `${$(elem).text()}`
      })

      description = description.split(' ').join('')
      if (description.length >= 499) {
        description = description.substring(0, 499)
      }

      let companyName = ""
      $('div.job-details.row > div.col-xs-12.col-sm-12.col-md-9 > article > div > header > div.col-xs-12 > div > span > a > span').each((i, elem) => {
        companyName += `${$(elem).text()}`
      })

      let companyUrl = ""
      $('div.job-details.row > div.col-xs-12.col-sm-12.col-md-9 > article > div > header > div.col-xs-12 > div > span > a').each((i, elem) => {
        companyUrl += `${$(elem).attr('href')}`
      })
      // const [company, created] = await Company.findOrCreate({
      //   where: { url: companyUrl },
      //   defaults: { companyName, url: companyUrl }
      // });
      const vacancy = await Vacancy.create({ companyId: company.id, title, url: urls[i], description })
    }
    catch (e) {
      console.log(e)
    }
  }
}

// считываем все с бд и передаем параметры в воркеры
async function findScammers(req, res) {

  if (!isWorking) {
    isWorking = true
    const vacancies = await Vacancy.findAll()
    let counter = 0;
    for (let i = 0; i < vacancies.length; i++) {
      const result = await pool.exec({ id: vacancies[i].id, title: vacancies[i].title, url: vacancies[i].url, description: vacancies[i].description });
      counter++;
      if (counter === vacancies.length) {
        await pool.destroy()
        isWorking = false
      }
    }

  } else if (isWorking) {
    res.status(200).json({ isWorking: isWorking })
  }
}

//получаем результаты по post запросу в котором передаем url компании и возвращаем массив обьектов
async function getResults(req, res) {

  const result = await Company.findAll({
    where: { url: req.body.url },
    include: [{
      model: Vacancy,
      include: [{
        model: Scammer,  
      // не разобрался в обьекте
      }]
    }]
  })



  console.log("!-------+++++--------!", result[0])

  let arrResults = []
  // arrResults.push(
  //   {
  //     "client_name": companyName,
  //     "client_url": companyUrl,
  //     "parent_vacancy_url": 53942538,

  //     "scammer_vacancy_url": 66181893,
  //     "scammer_company_name": 5124702,
  //     "scammer_company_url": "",

  //     "name": "Продавец-консультант/Представитель бренда бездымных систем",
  //     "url": "https://hh.ru/vacancy/66181893",
  //     "text": "Обладаешь развитыми навыками коммуникации. Доброжелательно относишься к людям. Готов работать полный рабочий день. Имеешь желание или опыт работы в сфере... Работать с входящим потоком клиентов. Консультировать клиентов по товарам. Осуществлять продажи продукта и аксессуаров. Вести отчетность.",
  //     "created_at": "2022-06-14 00:00:00",
  //     "updated_at": "2022-06-14 00:00:00"
  //   }
  // )
  res.status(200).json({ arrResults })
}

module.exports = {
  addCompanies, findScammers, getResults
}