module.exports = (sequelize, Sequelize) => {
    const Vacancy = sequelize.define("table_vacancies", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },

        companyId: {
            type: Sequelize.INTEGER,
        },

        title: {
            type: Sequelize.STRING
        },
        url: {
            type: Sequelize.STRING
        },
        description: {
            type: Sequelize.STRING(500)
        },
        
        
    },
    {
        indexes: [
            // Create a unique index on email
            {
                unique: true,
                fields: ['url']
            }
        ]
    })

    return Vacancy
}