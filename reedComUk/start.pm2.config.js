// Запуск этого приложения: pm2 start site.config.js

module.exports = {
    apps : [{
        name: "reedcom",
        script: "./app.js",
        env: {
            NODE_ENV: "production",
            WORKING: 0,
            PORT: 5000
        },
        env_dev: {
            NODE_ENV: "production",
            WORKING: 0,
            PORT: 5000
        },
        // instances : "2",
        // exec_mode : "cluster"
    }]
}