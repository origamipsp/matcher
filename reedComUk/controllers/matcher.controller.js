const { StaticPool } = require('node-worker-threads-pool')
const os = require('os')
const db = require('../models/index.js')
//const fs = require('fs')
const path = require('path')
const axios = require('axios')
const cheerio = require('cheerio')
const { Op } = require("sequelize");
//const { table } = require('console')


const Vacancy = db.vacancy
const Company = db.company
const Scammer = db.scammer



//путь к воркеру
const filePath = path.join(path.dirname(require.main.filename), 'workers', 'worker_reed.js');
const numCPUs = os.cpus().length



//получаем количество страниц для парсинга
async function getPagesCount(agency) {
  try {
    const html = await axios.get(`https://www.reed.co.uk/jobs/${agency}?pageno=1`)
    const $ = cheerio.load(html.data)
    let jobsCount = ""
    $('div.row.search-results > div.col-sm-8.col-md-9.results-container > div.search-results-footer > div:nth-child(1) > div.row-top.hidden-xs > div > div > div').each((i, elem) => {
      jobsCount += `${$(elem).text()}`
    })
    jobsCount = jobsCount.replace(/\s/g, '');
    let start = jobsCount.indexOf("f")
    let end = jobsCount.indexOf("j")
    jobsCount = jobsCount.slice(start + 1, end)
    jobsCount = parseInt(jobsCount)
    let pagesCount = Math.ceil(jobsCount / 25)
    return pagesCount

  } catch (error) {
    console.log(error)
  }
}

//getPagesCount("experis-ltd/p24927")


//парсим название компании и записываем в бд
async function parseCompanyName(agency) {
  console.log(agency)
  try {
    const html = await axios.get(`https://www.reed.co.uk/company-profile/${agency}`)
    const $ = cheerio.load(html.data)
    let companyName = ""
    $('div:nth-child(2) > div > section.profile__information > h1 > span').each((i, elem) => {
      companyName += `${$(elem).text()}`
    })
    companyName = companyName.replace("Working at ", "")
    const [company, created] = await Company.findOrCreate({
      where: { url: agencyArr },
      defaults: { companyName, url: agencyArr }
    });
  } catch (error) {
    console.log(error)
  }
}

//парсим url всех вакансий нужной компании, закидываем их в массив
async function addCompanies(req, res) {
  try {
    console.log(req.body.companies)
    let urlList = []
    let agencyArr = req.body.companies
    for (let j = 0; j < agencyArr.length; j++) {

      //await parseCompanyName(agencyArr[j]) // вынесен парсинг названия компании и запись в бд

      const pageCount = await getPagesCount(agencyArr[j])

      for (let i = 1; i <= pageCount; i++) {

        try {
          const html = await axios.get(`https://www.reed.co.uk/jobs/${agencyArr[j]}?pageno=${i}`)
          console.log('add - ', agencyArr[j])
          const $ = cheerio.load(html.data)
          $('div > div.col-sm-12.col-md-9.details > header > h3 > a').each((i, elem) => {
            let cutUrl = `https://www.reed.co.uk${$(elem).attr('href')}`
            //console.log("!!!!!!!!cutUrl- ", cutUrl)
            let symbol = cutUrl.indexOf("?")
            cutUrl = cutUrl.substring(0, symbol)
            urlList.push(cutUrl)
          })

          // let companyName = ""
          // $('div > div.col-sm-12.col-md-9.details > header > div.job-result-heading__posted-by > a').each((i, elem) => {
          //   companyName = `https://www.reed.co.uk${$(elem).text()}`
          // })

          // const [company, created] = await Company.findOrCreate({
          //   where: { url: agencyArr[j] },
          //   defaults: { companyName, url: agencyArr[j] }
          // });
        }
        catch (e) {
          console.log(e)
        }
      }
    }
    await parseAll(urlList) // сдесь передаем все url вакансий, всех компаний переданных в post запросе
    res.status(200).json({ message: 'ok' })
  } catch (error) {
    res.status(500).json({ message: error.message })
  }
}

//парсим всё нужное нам содержимое из каждой url массива
async function parseAll(urls) {

  for (let i = 0; i < urls.length; i++) {

    try {
      const html = await axios.get(urls[i])
      const $ = cheerio.load(html.data)

      let title = ""
      $('div.job-details.row > div.col-xs-12.col-sm-12.col-md-9 > article > div > header > div.col-xs-12 > h1').each((i, elem) => {
        title += `${$(elem).text()}`
      })
      //Удаляем мусор из тайтла и приводим его к необходимому виду
      // пример "Delivery Manager/ Lead", в запросе это должно выглядеть так "Delivery-Manager-Lead"
      title = title.replace(/[^a-zа-яё0-9\s]/gi, '').split(' ').filter(Boolean).join('-')

      let description = ""
      $('div.job-details.row > div.col-xs-12.col-sm-12.col-md-9 > article > div > div.description-container.row > div > div.description > span').each((i, elem) => {
        description += `${$(elem).text()}`
      })

      description = description.split(' ').join('')
      if (description.length >= 499) {
        description = description.substring(0, 499)
      }

      let companyName = ""
      $('div.job-details.row > div.col-xs-12.col-sm-12.col-md-9 > article > div > header > div.col-xs-12 > div > span > a > span').each((i, elem) => {
        companyName += `${$(elem).text()}`
      })

      let companyUrl = ""
      $('div.job-details.row > div.col-xs-12.col-sm-12.col-md-9 > article > div > header > div.col-xs-12 > div > span > a').each((i, elem) => {
        companyUrl += `${$(elem).attr('href')}`
      })

      const [company, created] = await Company.findOrCreate({
        where: {
          url: companyUrl
          // [Op.or]: [
          //   { url: companyUrl },
          //   { companyName }
          // ]
        },
        defaults: { companyName, url: companyUrl }
      });

      const vacancy = await Vacancy.create({ companyId: company.id, title, url: urls[i], description })

    } catch (e) {
      console.log(e)
    }
  }
}





// считываем все с бд и передаем параметры в воркеры
async function findScammers(req, res) {

  //создаем пулл воркеров
  const pool = new StaticPool({
    size: 2,
    task: filePath,
    workerData: 'workerData!'
  });

  try {
    if (!+process.env.WORKING) {
      process.env.WORKING = 1
      const start = new Date().getTime();
      const vacancies = await Vacancy.findAll()
      const companies = await Company.findAll()
      let companiesNames = []
      let counter = 0;
      for (let j = 0; j < companies.length; j++) {
        companiesNames.push(companies[j].dataValues.companyName)
      }

      for (let i = 0; i < vacancies.length; i++) {

        (async () => {

          const result = await pool.exec({ id: vacancies[i].id, title: vacancies[i].title, url: vacancies[i].url, description: vacancies[i].description, companies: companiesNames });
          counter++;
          if (counter >= vacancies.length - 1) {
            await pool.destroy()
            process.env.WORKING = 0
            const end = new Date().getTime();
            console.log(`SecondWay: ${end - start}ms`);
          }
        })();
      }



      res.status(200).json({ message: 'Started' })
    } else if (process.env.WORKING) {
      res.status(200).json({ message: "Working..." })
    }
  } catch (error) {
    res.status(500).json({ message: error.message })
  }
}

//получаем результаты по post запросу в котором передаем url компании и возвращаем массив обьектов
async function getResults(req, res) {

  try {
    const result = await Company.findAll({
      where: { url: req.body.url },
      include: [{
        model: Vacancy,
        include: [{
          model: Scammer,
        }]
      }]
    })
    // result[0].dataValues.companyName - название компании заказчика
    // result[0].dataValues.url - юрл компании заказчика
    // result[0].dataValues.table_vacancies[0].dataValues.url - юрл родительской вакансии заказчика
    // result[0].dataValues.table_vacancies[0].dataValues.table_scammers[0].dataValues.url - url вакансии скамера
    // result[0].dataValues.table_vacancies[0].dataValues.table_scammers[0].dataValues.companyName - название компании скамера
    let results = []
    for (let i = 0; i < result.length; i++) {
      for (let j = 0; j < result[i].dataValues.table_vacancies.length; j++) {
        for (let k = 0; k < result[i].dataValues.table_vacancies[j].dataValues.table_scammers.length; k++) {

          if (typeof result[i].dataValues.table_vacancies[j].dataValues.table_scammers[k] !== 'undefined') {
            results.push(
              {
                "clientName": result[i].dataValues.companyName,
                "clientUrl": result[i].dataValues.url,
                "parentVacancyUrl": result[i].dataValues.table_vacancies[j].dataValues.url,
                "scammerVacancyUrl": result[i].dataValues.table_vacancies[j].dataValues.table_scammers[k].dataValues.url,
                "scammerCompanyName": result[i].dataValues.table_vacancies[j].dataValues.table_scammers[k].dataValues.companyName,
              }
            )
          } else {
            results.push(
              {
                "clientName": result[i].dataValues.companyName,
                "clientUrl": result[i].dataValues.url,
                "parentVacancyUrl": result[i].dataValues.table_vacancies[j].dataValues.url,
                "scammerVacancyUrl": "",
                "scammerCompanyName": "",
              }
            )
          }
        }
      }
    }


    res.status(200).json({ results })
  } catch (error) {
    res.status(500).json({ message: error.message })
  }
}

module.exports = {
  addCompanies, findScammers, getResults
}