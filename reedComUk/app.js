const db = require('./models/index.js')
const express = require('express');


    (async () => {
        await db.sequelize.sync()
    })();

const urlencodedParser = express.urlencoded({ extended: false });
const PORT = process.env.PORT ?? 3000
const app = express()

app.use(express.json({ extended: true }))
app.use('/api/matcher', require('./routes/matcher.routes'))

app.listen(PORT, () => {
    console.log(`+++++Server has been started on port ${PORT}...+++++`)
})



