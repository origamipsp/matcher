const { parentPort } = require('worker_threads')
const axios = require('axios')
const cheerio = require('cheerio')
const { distance } = require('fastest-levenshtein')
const db = require('../models/index.js')
const { Op } = require("sequelize");
const c = require('ansi-colors');
const CLIInfinityProgress = require('cli-infinity-progress');

const progress = new CLIInfinityProgress();
const Scammer = db.scammer
const min = 100
const max= 195

const delay = ms => new Promise(resolve => setTimeout(resolve, ms))
function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

// получаем количество страниц потенциальных скамеров, удовлетворяющих поиску по тайтлу искомой вакансии заказчика
async function getPagesCount(title) {
  await delay(getRandomInt(min, max))
  try {
    const html = await axios.get(`https://www.reed.co.uk/jobs/${title}-jobs`)
    const $ = cheerio.load(html.data)
    let jobsCount = ""
    $('div.row.search-results > div.col-sm-8.col-md-9.results-container > div.search-results-footer > div:nth-child(1) > div.row-top.hidden-xs > div > div > div').each((i, elem) => {
      jobsCount += `${$(elem).text()}`
    })
    jobsCount = jobsCount.replace(/\s/g, '');
    let start = jobsCount.indexOf("f")
    let end = jobsCount.indexOf("j")
    jobsCount = jobsCount.slice(start + 1, end)
    jobsCount = parseInt(jobsCount)
    //console.log(jobsCount)
    let pagesCount = Math.ceil(jobsCount / 25)
    //console.log(pagesCount)
    return pagesCount

  } catch (error) {
    console.log(`getPagesCount error: ${error.message}`)
  }
}



//парсим url всех потенциальных скамеров, закидываем их в массив  obj = 1 вакансия
async function parseJobsUrl(obj) {
 
  //progress.setHeader('Working ...').start();
  console.log("Worker is working...")
  const pageCount = await getPagesCount(obj.title)
  let potentialScammerList = []
  for (let i = 1; i <= pageCount; i++) {

    try {
      await delay(getRandomInt(min, max))
      const html = await axios.get(`https://www.reed.co.uk/jobs/${obj.title}-jobs?pageno=${i}`)
      const $ = cheerio.load(html.data)
      $('div > div.col-sm-12.col-md-9.details > header > h3 > a').each((i, elem) => {

        let cutUrl = `https://www.reed.co.uk${$(elem).attr('href')}`
        let symbol = cutUrl.indexOf("?")
        cutUrl = cutUrl.substring(0, symbol)

        if (obj.url !== cutUrl) {

          potentialScammerList.push(cutUrl)
        }
      })


    } catch (error) {
      console.log(`ParseJobsUrl error: ${error.message}`)
    }
  }
  await parseAllFrompotentialScammer(potentialScammerList, obj)
}

//парсим всё нужное нам содержимое  из каждой url массива потенциальных скамеров
async function parseAllFrompotentialScammer(urls, obj) {

  for (let i = 0; i < urls.length; i++) {
    
    try {
      await delay(getRandomInt(min, max+350))
      const html = await axios.get(urls[i])
      const $ = cheerio.load(html.data)

      let title = ""
      $('#content > div.job-details.row > div.col-xs-12.col-sm-12.col-md-9 > article > div > header > div.col-xs-12 > h1').each((i, elem) => {
        title += `${$(elem).text()}`
      })
      let description = ""
      $('#content > div.job-details.row > div.col-xs-12.col-sm-12.col-md-9 > article > div > div.description-container.row > div > div.description > span').each((i, elem) => {
        description += `${$(elem).text()}`
      })

      description = description.split(" ").join('')
      if (description.length >= 499) {
        description = description.substring(0, 499)
      }

      let companyName = ""
      $('#content > div.job-details.row > div.col-xs-12.col-sm-12.col-md-9 > article > div > header > div.col-xs-12 > div > span > a > span').each((i, elem) => {
        companyName += `${$(elem).text()}`
      })
      // let companyUrl = ""
      // $('#content > div.job-details.row > div.col-xs-12.col-sm-12.col-md-9 > article > div > header > div.col-xs-12 > div > span > a').each((i, elem) => {
      //   companyUrl += `${$(elem).attr('href')}`
      // })

      // проверяем расстояние левинштайна между описанием вакансси заказчика и описанием каждого потенциального заказчика
      // если удовлетворяет условию - записываем в бд найденного скамера
      let diff = distance(obj.description, description) / obj.description.length
      // console.log("includes___________: ", obj.companies.includes(companyName))
      // console.log("scammerName________: ", companyName)
      // console.log("companyName________: ", obj.companies)
      if (diff < 0.3 && !obj.companies.includes(companyName)) {
        console.log(c.red('SCAMMER!!!!!!!!!!!!!!!!!!!!!!!!!!'));
        const [scammer, created] = await Scammer.findOrCreate({
          where: {
            //url: urls[i]
            [Op.or]: [
              { url: urls[i] },
              { companyName: companyName }
            ]
          },
          defaults: { vacancyId: obj.id, url: urls[i], companyName }
        });

      }
    }
    catch (e) {
      console.log(`parseAllFrompotentialScammer error: ${e.message}`)
    }
  }
}

// Основной поток передаст нужные вам данные
// через этот прослушиватель событий.
parentPort.on('message', async (param) => {
  const result = await parseJobsUrl(param);
  // Access the workerData.
  // return the result to main thread.
  parentPort.postMessage("feels good");
});
